package com.automation;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Application {
    public static void main(String[] args){


        try{
            myError();
            myNullPointerException();
            myIndexOutOfBoundsException();
            myNumberFormatException();
            myClassCastException();
            myIllegalArgumentException(0, 2);
            myIllegalStateException();
            myUnsupportedOperationException();

            myClassNotFoundException();
            myFileNotFound("c:\1.txt");

        } catch (Throwable e){
            e.printStackTrace();
        }
    }

    private static void myError() throws StackOverflowError{
        myError();
    }

    private static void myFileNotFound(String file) throws FileNotFoundException {
        FileReader file1 = new FileReader(file);
    }
    private static void myNullPointerException() throws NullPointerException{
            String s = null;
            System.out.println(s.toUpperCase());
    }

    private static void myIndexOutOfBoundsException() throws IndexOutOfBoundsException{
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        System.out.println(list.get(20));
    }

    private static void myNumberFormatException() throws NumberFormatException{
        String s = "123q";
            System.out.println(Integer.parseInt(s));
    }

    private static void myClassCastException() throws ClassCastException{
        Object s = new String("123");
        System.out.println((int) s);
    }

    private static void myClassNotFoundException() throws ClassNotFoundException{
        Class c;
        c = Class.forName("MyClass");
    }

    private static int myIllegalArgumentException(int a, int b) throws IllegalArgumentException{
        if (a == 0 || b == 0)
            try {
                throw new IllegalArgumentException("numbers must be positive");
            } catch (IllegalArgumentException e) {
                System.out.println(e);
            }

        return a * b;
    }

    private static void myIllegalStateException() throws IllegalStateException{
        new ArrayList<>().iterator().remove();
    }

    private static void myUnsupportedOperationException() throws UnsupportedOperationException{
        List<Integer> list1 = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        List<Integer> list2 = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        list1.addAll(list2);
    }

}